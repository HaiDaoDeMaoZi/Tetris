# Tetris

#### 项目介绍
2048+汉字俄罗斯方块小游戏Android app，将现有的2048 俄罗斯方块小游戏以 汉字合成而不是 2048 合成的方式实现，由于现有的市场上 并不存在同种小游戏，这种汉字的组成方式更加有趣和游戏体验者可以在玩游戏的时候体会到汉字组成的神秘之处。

#### 安装教程

[点击此链接](https://gitee.com/HaiDaoDeMaoZi/Tetris/blob/master/app/release/app-release.apk)下载小游戏的apk，将apk下载于安卓系统的手机中即可进行软件的安装


#### 软件截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/221548_e6e72d20_2228314.png "微信截图_20181113221412.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/221501_c067c376_2228314.png "中途.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220406_9eb4ad95_2228314.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220431_f4028f85_2228314.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220442_fa8fbe2f_2228314.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220453_d90ba272_2228314.png "7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220509_3a5c4083_2228314.png "9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/220523_b7f2ec79_2228314.png "10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/221517_c953a461_2228314.png "崩.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/221533_5d81b1a0_2228314.png "崩1.png")
